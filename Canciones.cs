/* (4.3.2.1) Amplia el programa del ejercicio 4.3.1.1,
para que almacene datos de hasta 100 c[
Deberá tener un menú que permita las opciones: 
añadir una nueva canción, mostrar el título de todas las c[
buscar la canción que contenga un cierto texto (en el artista o en el título).*/

using System;
using System.Collections.Generic;
//using System.Ling;
using System.Text;
using System.Threading.Tasks;


public class Canciones {

 int contador = 0;


 struct tipoCanciones { 

        public string nombre;
        public string album; 
        public int año;
        public string artista; 
       
        } 

tipoCanciones [] c = new tipoCanciones[100];

public Canciones (int contador){

  this.contador = contador;

}        

public void Añadir(int contador){ 
  
this.contador = contador;

Console.WriteLine("ESTE PROGRAMA LE PERMITIRA GUARDAR HASTA 100 CANCIONES");

for(int i=0; i<this.c.Length; i++){ 
Console.WriteLine("Digite el nombre de la cancion");
this.c[i].nombre = (Console.ReadLine());
Console.WriteLine("Digite el album de la cancion");
this.c[i].album = (Console.ReadLine());
Console.WriteLine("Digite el año de la cancion");
this.c[i].año = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("Digite el artista de la cancion");
this.c[i].artista = (Console.ReadLine());
Console.WriteLine("Desea seguir guardando canciones?: SI/NO");
  string r = Console.ReadLine();

    if(r=="Si" || r=="si" || r=="SI"){
        this.contador++; 
    
    }else{
      break;
    }

    }

     Menu();

 }



public void Buscar(){ 

Console.WriteLine("Digite el artista de la cancion a buscar");
string art = Console.ReadLine();
Console.WriteLine("Digite el titulo de la cancion a buscar");
string titulo = Console.ReadLine();


for(int i=0; i<c.Length; i++){ 

if (this.c[i].artista.Contains(art) || this.c[i].nombre.Contains(titulo) ){  

Console.WriteLine("La cancion se encuentra en la posicion{0}", i);
Console.WriteLine("Los datos completos de la cancion son:");

Console.WriteLine("Nombre: {0}", this.c[i].nombre);
Console.WriteLine("Album: {0}",this.c[i].album);
Console.WriteLine("Año: {0}", this.c[i].año);
Console.WriteLine("Artista: {0}", this.c[i].artista);

Console.ReadLine();


}

 Console.WriteLine("Desea seguir buscando canciones?: SI/NO");
 string r = Console.ReadLine();


  if(r=="No" || r=="no" || r=="NO"){

  Menu();

  }
   

   }
 }



public void Mostrar(){

Console.WriteLine("Desea ver las canciones almacenadas en su lista? digite SI/NO");

string resp = Console.ReadLine();

if (resp =="SI" || resp == "si" || resp == "Si"){
  Console.WriteLine("[*[*[*[*[ Las canciones almacenadas en su lista son *]*]*]*]*] ");
      for (int j = 0; j <= contador; j++)
       {
         Console.WriteLine("*******************************");
        Console.WriteLine("Cancion #: {0}", j+1);
        Console.WriteLine("Nombre: {0}", this.c[j].nombre);
        Console.WriteLine("Album: {0}", this.c[j].album);
        Console.WriteLine("Año: {0}", this.c[j].año);
        Console.WriteLine("Artista: {0}", this.c[j].artista);


        }
  Console.WriteLine("|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*");
   }

    Console.WriteLine("Desea volver al menu principal? SI/NO");
    string r = Console.ReadLine();


      if(r=="SI" || r=="si" || r=="Si"){

      Menu();

      }
  } 


public void Menu(){
Console.WriteLine("BIENVENIDO AL MENU DEL PROGRAMA CANCIONES");
Console.WriteLine("1). Añadir una nueva cancion");
Console.WriteLine("2). Mostrar el titulo de todas las canciones");
Console.WriteLine("3). Buscar una cancion");
Console.WriteLine("4). Salir");

 string r = Console.ReadLine();

switch (r) {

    case "1":

     Añadir(0);

    break;

    case "2":
      
     Mostrar();
    break;

    case "3":

     Buscar();
    
    break;

    case "4":
    
    break;

    default:

    Console.WriteLine("Ha salido del programa");

    break;


  

   }
}
 

class Canciones2
{
  
public static void Main(){
Canciones obj = new Canciones(0);
obj.Menu();

 }

}

}






